import { Request, Response, Router } from "express";

const router = Router();

/**
 * http://localhost:3002/blogs [GET]
 */
router.get('/', (req: Request, res: Response)  => {
  res.send({data: 'Blogs'})
})

export { router };