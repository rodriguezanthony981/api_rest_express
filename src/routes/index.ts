import { Router } from "express";
import { readdirSync } from "fs";

const PATH_ROUTER = `${__dirname}`;
const router = Router();

/**
 * 
 * @param fileName 
 * @returns 
 */

const cleanFileName = (fileName: string): string => {
  const file = fileName.split('.').shift()
  return file ?? 'index';
}

readdirSync(PATH_ROUTER).forEach((fileName) => {
  const cleanName = cleanFileName(fileName);

  if(cleanName !== 'index'){
    import(`./${cleanName}`).then((moduleRouter) => {
      console.log(`Se esta cargando la ruta.... ${cleanName}`);
      router.use(`/${cleanName}`, moduleRouter.router);
    });
  }
  
})


export { router };