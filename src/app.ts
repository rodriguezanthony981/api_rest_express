import "dotenv/config"
import express from "express";
import cors from "cors";

//* Routers
import { router } from "./routes";
import db from "./config/mongo"

import morgan from 'morgan';

const app = express()
const PORT = process.env.PORT ?? 3001;

//** MidleWare para Cors
app.use(cors())

//* MidleWare
app.use(express.json());
app.use(morgan('dev'));

//* Rutas
app.use(router);

db().then(() => console.log("Conexión Ready"));

app.listen(PORT, () => console.log(`Listo por el puerto ${PORT}`));

