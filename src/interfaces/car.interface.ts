export interface Car {
  color: string;
  gas: gas;
  year: number;
  description: string;
  price: number;
  name: string;
}

enum gas {
  "gasoline",
  "electric"
}